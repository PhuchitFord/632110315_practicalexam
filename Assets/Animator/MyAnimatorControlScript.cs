using UnityEngine;

namespace Samarnggoon.GameDev3.Chapter8
{
 public class MyAnimatorControlScript : MonoBehaviour
 {
  protected Animator m_Animator;

  private static readonly int Punch = Animator.StringToHash("Punch");
  private static readonly int Dancing = Animator.StringToHash("Dancing");
  private static readonly int State = Animator.StringToHash("State");
  private static readonly int Turn = Animator.StringToHash("Turn");
  private static readonly int Taunt = Animator.StringToHash("Taunt");
  private static readonly int Kick = Animator.StringToHash("Kick");
  private static readonly int Vic = Animator.StringToHash("Vic");
  
  private void Start()
  {
   m_Animator = GetComponent<Animator>();
  }

  void Update()
  {
   if (Input.GetKeyDown(KeyCode.V))
   {
    m_Animator.SetTrigger(Taunt);
   }

   if (Input.GetKeyDown(KeyCode.B))
   {
    m_Animator.SetTrigger(Punch);
   }

   if (Input.GetKeyDown(KeyCode.X))
   {
    m_Animator.SetBool(Dancing, true);
   }
   

   
 

   //---------------------------------


   if (Input.GetKeyDown(KeyCode.W))
   {
    m_Animator.SetInteger(State, 3);
   }
   
   if (Input.GetKeyDown(KeyCode.Q))
   {
    m_Animator.SetInteger(State, 2);
   }
   
   if (Input.GetKeyDown(KeyCode.N))
   {
    m_Animator.SetTrigger(Kick);
   }
   
   if (Input.GetKeyDown(KeyCode.M))
   {
    m_Animator.SetTrigger(Vic);
   }

   
  }

  public void Z ()
  {
   m_Animator.SetTrigger(Punch);
  }
  
  public void A ()
  {
   m_Animator.SetTrigger(Kick);
  }
  
  public void Q ()
  {
   m_Animator.SetInteger(State, 2);
  }
  
  public void W ()
  {
   m_Animator.SetInteger(State, 3);
  }
  
  public void S ()
  {
   m_Animator.SetTrigger(Taunt);
  }
  
  public void X ()
  {
   m_Animator.SetBool(Dancing, true);
  }
  
  public void V ()
  {
   m_Animator.SetFloat(Turn, 0.3f);
  }
  
  public void C ()
  {
   m_Animator.SetTrigger(Vic);
  }
  
  
 }
}
 
