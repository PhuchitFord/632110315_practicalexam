using System;
using UnityEngine;

namespace Samarnggoon.GameDev3.Chapter4
{
     public class ItemTriggerWithPlayerHandler : MonoBehaviour
{
     protected virtual void OnTriggerEnter(Collider other)
 {
     if (other.CompareTag("Player")){
         if (gameObject.CompareTag("Diamond"))
             {
             ProcessTriggerWithDiamond();
             }else if (gameObject.CompareTag("Score"))
             {
             ProcessTriggerWithCoin();
             }
         //Get the Inventory component from the player
         var inventory = other.GetComponent<Inventory>();
         //Add the collected item's tag name to the inventory
         inventory.AddItem(gameObject.tag,100);
        
        //Destroy itself
         Destroy(gameObject);
         }
     }

 protected virtual void ProcessTriggerWithDiamond()
 {
    
     }
 protected virtual void ProcessTriggerWithCoin()
 {
    
     }
 }
 }