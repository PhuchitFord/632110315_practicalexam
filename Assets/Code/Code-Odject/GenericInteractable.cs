using Samarnggoon.GameDev3.Chapter5.InteractionSystem;
using UnityEngine;
using UnityEngine.Events;

namespace Samarnggoon.GameDev3.Chapter6.UnityEvents
{
    public class GenericInteractable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected UnityEvent m_OnInteract = new();
        [SerializeField] protected UnityEvent m_OnActorEnter = new();
        [SerializeField] protected UnityEvent m_OnActorExit = new();

        public virtual void Interact()
        {
            m_OnInteract.Invoke();
        }

        public virtual void ActorEnter()
        {
            m_OnActorEnter.Invoke();
        }

        public virtual void ActorExit()
        {
            m_OnActorExit.Invoke();
        }
    }
}
