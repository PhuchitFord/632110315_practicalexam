using UnityEngine;

namespace Samarnggoon.GameDev3.Chapter5.InteractionSystem
{
public interface IInteractable
{
void Interact();
}
}
