namespace Samarnggoon.GameDev3.Chapter5.InteractionSystem
{
    public interface IActorEnterExitHandler
    {
        void ActorEnter();
        void ActorExit();
    }

}
