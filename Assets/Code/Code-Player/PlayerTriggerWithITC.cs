using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Samarnggoon.GameDev3.Chapter1;
using UnityEngine;

namespace Samarnggoon.GameDev3.Chapter4
 {
     public class PlayerTriggerWithITC : MonoBehaviour
 {
     private void OnTriggerEnter(Collider other)
     {
         //Get components from item object
         //Get the ItemTypeComponent component from the triggered object
         ItemTypeComponent itc = other.GetComponent<ItemTypeComponent>();
        
        //Get components from the player
         //Inventory
         var inventory = GetComponent<Inventory>();
         //SimpleHealthPointComponent
         var simpleHP = GetComponent<SimpleHealthPointComponent>();
        
         if (itc !=null)
             {
             switch (itc.Type)
             {
                 case ItemType.COIN:
                 inventory.AddItem("Score",100);
                 break;
                 case ItemType.BIGCOIN:
                 inventory.AddItem("Score",-100);
                 break;
                 case ItemType.POWERUP:
                        inventory.AddItem("Score", 500);
                        break;
                 case ItemType.POWERDOWN:
                        inventory.AddItem("Score", -500);

                        break;
            }
             Destroy(other.gameObject,0);
             }
        
         }
     }
 }